# Barebones for a new Polymer 3 project

## Install the Polymer-CLI

First, make sure you have the [Polymer CLI](https://www.npmjs.com/package/polymer-cli) installed. 

## Run your Application

This project is using package.json and it has some scripts defined to make it run, view that file to know different scripts

Following is an example of most basic script to run your application.

    $ npm run dev

## Building Your Application

    $ polymer build

This will create builds of your application in the `build/` directory, optimized to be served in production. You can then serve the built versions by giving `polymer serve` a folder to serve from:

    $ polymer serve build/default

TODO: Add scripts to package.json for building application

## Running Tests

    $ polymer test

Your application is already set up to be tested via [web-component-tester](https://github.com/Polymer/web-component-tester). Run `polymer test` to run your application's test suite locally.

TODO: Add scripts to package.json for building application
